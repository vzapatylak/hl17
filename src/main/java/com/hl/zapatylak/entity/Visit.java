package com.hl.zapatylak.entity;

public class Visit {
	
	int id;
	int user;
	int location;
	long visited_at;
	int mark;
	
	public Visit() {
		super();
	}

	public Visit(int id, int userId, int locationId, long visitedAt, int mark) {
		super();
		this.id = id;
		this.user = userId;
		this.location = locationId;
		this.visited_at = visitedAt;
		this.mark = mark;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return user;
	}

	public void setUserId(int userId) {
		this.user = userId;
	}

	public int getLocationId() {
		return location;
	}

	public void setLocationId(int locationId) {
		this.location = locationId;
	}

	public long getVisitedAt() {
		return visited_at;
	}

	public void setVisitedAt(long visitedAt) {
		this.visited_at = visitedAt;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + location;
		result = prime * result + mark;
		result = prime * result + user;
		result = prime * result + (int) (visited_at ^ (visited_at >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Visit other = (Visit) obj;
		if (id != other.id)
			return false;
		if (location != other.location)
			return false;
		if (mark != other.mark)
			return false;
		if (user != other.user)
			return false;
		if (visited_at != other.visited_at)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Visit [id=" + id + ", userId=" + user + ", locationId=" + location + ", visitedAt=" + visited_at
				+ ", mark=" + mark + "]";
	}
	
	
	

}
