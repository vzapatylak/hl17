package com.hl.zapatylak.entity;

public class CutVisit {

	String place;
	long visited_at;
	int mark;

	public CutVisit() {
		super();
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public long getVisited_at() {
		return visited_at;
	}

	public void setVisited_at(long visited_at) {
		this.visited_at = visited_at;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mark;
		result = prime * result + ((place == null) ? 0 : place.hashCode());
		result = prime * result + (int) (visited_at ^ (visited_at >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CutVisit other = (CutVisit) obj;
		if (mark != other.mark)
			return false;
		if (place == null) {
			if (other.place != null)
				return false;
		} else if (!place.equals(other.place))
			return false;
		if (visited_at != other.visited_at)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "cutVisit [place=" + place + ", visited_at=" + visited_at
				+ ", mark=" + mark + "]";
	}

}
