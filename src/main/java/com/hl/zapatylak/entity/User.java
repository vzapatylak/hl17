package com.hl.zapatylak.entity;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

public class User {

	int id;
	long birth_date;
	String email;
	String gender;
	String first_name;
	String last_name;

	public User() {
		super();
	}

	public User(int id, long birthDate, String email, String gender, String firstName, String lastName) {
		super();
		this.id = id;
		this.birth_date = birthDate;
		this.email = email;
		this.gender = gender;
		this.first_name = firstName;
		this.last_name = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getBirthDate() {
		return birth_date;
	}

	public void setBirthDate(long birthDate) {
		this.birth_date = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return first_name;
	}

	public void setFirstName(String firstName) {
		this.first_name = firstName;
	}

	public String getLastName() {
		return last_name;
	}

	public void setLastName(String lastName) {
		this.last_name = lastName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (birth_date ^ (birth_date >>> 32));
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((first_name == null) ? 0 : first_name.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + id;
		result = prime * result + ((last_name == null) ? 0 : last_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (birth_date != other.birth_date)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (first_name == null) {
			if (other.first_name != null)
				return false;
		} else if (!first_name.equals(other.first_name))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id != other.id)
			return false;
		if (last_name == null) {
			if (other.last_name != null)
				return false;
		} else if (!last_name.equals(other.last_name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", birthDate=" + birth_date + ", email=" + email + ", gender=" + gender
				+ ", firstName=" + first_name + ", lastName=" + last_name + "]";
	}

	public String toJson() {
		Map map = new HashMap();
		map.put("id", id);
		map.put("first_name", first_name);
		map.put("last_name", last_name);
		map.put("birth_date", birth_date);
		map.put("gender", gender);
		map.put("email", email);
		String str = new Gson().toJson(map);

		return str;
	}

}
