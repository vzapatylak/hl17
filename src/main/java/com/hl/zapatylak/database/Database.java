package com.hl.zapatylak.database;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.gson.Gson;
import com.hl.zapatylak.entity.Location;
import com.hl.zapatylak.entity.User;
import com.hl.zapatylak.entity.Visit;

public class Database {

	public static ConcurrentHashMap<Integer, User> userTable = new ConcurrentHashMap<Integer, User>();
	public static ConcurrentHashMap<Integer, Location> locationTable = new ConcurrentHashMap<Integer, Location>();
	public static ConcurrentHashMap<Integer, Visit> visitTable = new ConcurrentHashMap<Integer, Visit>();
	public static ConcurrentHashMap<Integer, List<Visit>> visitTableByUser = new ConcurrentHashMap<Integer, List<Visit>>();
	public static ConcurrentHashMap<Integer, List<Visit>> visitTableByLocation = new ConcurrentHashMap<Integer, List<Visit>>();
	public static ConcurrentHashMap<String, Integer> scheme = new ConcurrentHashMap<String, Integer>();

	public static void init() {
		scheme.put("lastID", 0);
		String baseDir = "D:\\1\\FULL\\data";
		String[] types = { "users", "locations", "visits" };
		File[] files = new File(baseDir).listFiles();

		String content = null;

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.toString().contains("users")) {
				try {
					content = new String(Files.readAllBytes(Paths.get(file
							.toString())));
					Map map = new Gson().fromJson(content, Map.class);
					List list = (List) map.get("users");
					for (int j = 0; j < list.size(); j++) {
						User user = new User();
						user.setId(((Double) (((Map) list.get(j)).get("id")))
								.intValue());
						user.setBirthDate(((Double) ((Map) list.get(j))
								.get("birth_date")).longValue());
						user.setEmail(((Map) list.get(j)).get("email")
								.toString());
						user.setFirstName(((Map) list.get(j)).get("first_name")
								.toString());
						user.setLastName(((Map) list.get(j)).get("last_name")
								.toString());
						user.setGender(((Map) list.get(j)).get("gender")
								.toString());
						userTable.put(
								((Double) (((Map) list.get(j)).get("id")))
										.intValue(), user);
						updateScheme((Map) list.get(j));
					}
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.toString().contains("locations")) {
				try {
					content = new String(Files.readAllBytes(Paths.get(file
							.toString())));
					Map map = new Gson().fromJson(content, Map.class);
					List list = (List) map.get("locations");
					for (int j = 0; j < list.size(); j++) {
						Location location = new Location();
						location.setId(((Double) (((Map) list.get(j)).get("id")))
								.intValue());
						location.setDistance(((Double) (((Map) list.get(j))
								.get("distance"))).intValue());
						location.setCity(((Map) list.get(j)).get("city")
								.toString());
						location.setCountry(((Map) list.get(j)).get("country")
								.toString());
						location.setPlace(((Map) list.get(j)).get("place")
								.toString());
						locationTable.put(((Double) (((Map) list.get(j))
								.get("id"))).intValue(), location);
						updateScheme((Map) list.get(j));
					}
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.toString().contains("visits")) {
				try {
					content = new String(Files.readAllBytes(Paths.get(file
							.toString())));
					Map map = new Gson().fromJson(content, Map.class);
					List list = (List) map.get("visits");
					for (int j = 0; j < list.size(); j++) {
						Visit visit = new Visit();
						visit.setId(((Double) (((Map) list.get(j)).get("id")))
								.intValue());
						visit.setLocationId(((Double) (((Map) list.get(j))
								.get("location"))).intValue());
						visit.setUserId(((Double) (((Map) list.get(j))
								.get("user"))).intValue());
						visit.setMark(((Double) (((Map) list.get(j))
								.get("mark"))).intValue());
						visit.setVisitedAt(((Double) ((Map) list.get(j))
								.get("visited_at")).longValue());
						visitTable.put(
								((Double) (((Map) list.get(j)).get("id")))
										.intValue(), visit);
						updateScheme((Map) list.get(j));

						if (visitTableByUser.get(((Double) (((Map) list.get(j))
								.get("user"))).intValue()) == null) {
							CopyOnWriteArrayList<Visit> vList = new CopyOnWriteArrayList<Visit>();
							vList.add(visit);
							visitTableByUser
									.put(((Double) (((Map) list.get(j))
											.get("user"))).intValue(), vList);
						} else {
							CopyOnWriteArrayList<Visit> vList = (CopyOnWriteArrayList<Visit>) visitTableByUser
									.get(((Double) (((Map) list.get(j))
											.get("user"))).intValue());
							vList.add(visit);
							visitTableByUser
									.put(((Double) (((Map) list.get(j))
											.get("user"))).intValue(), vList);
						}

						if (visitTableByLocation.get(((Double) (((Map) list
								.get(j)).get("location"))).intValue()) == null) {
							CopyOnWriteArrayList<Visit> lList = new CopyOnWriteArrayList<Visit>();
							lList.add(visit);
							visitTableByLocation.put(((Double) (((Map) list
									.get(j)).get("location"))).intValue(),
									lList);
						} else {
							CopyOnWriteArrayList<Visit> lList = (CopyOnWriteArrayList<Visit>) visitTableByLocation
									.get(((Double) (((Map) list.get(j))
											.get("location"))).intValue());
							lList.add(visit);
							visitTableByLocation.put(((Double) (((Map) list
									.get(j)).get("location"))).intValue(),
									lList);
						}

					}
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}

	}

	public static void updateScheme(Map map) {
		if (scheme.get("lastID") < ((Double) (map.get("id"))).intValue()) {
			scheme.put("lastID", ((Double) (map.get("id"))).intValue());
		}
	}
}
