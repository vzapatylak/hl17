package com.example;

import org.rapidoid.annotation.Valid;
import org.rapidoid.jpa.JPA;
import org.rapidoid.log.Log;
import org.rapidoid.security.Auth;
import org.rapidoid.setup.App;
import org.rapidoid.setup.My;
import org.rapidoid.setup.On;
import org.rapidoid.u.U;

public class GettingStartedExample {

	public static void main(String[] args) {
		Log.info("Starting application");

	//	App.init(args).beans().jpa().auth(); // bootstrap beans (controllers, services etc.), JPA and Auth

		On.get("/books").json(() -> JPA.of(Book.class).all()); // get all books
		
		On.get("/books/{id}").json((Integer id) -> id);


	

		//App.ready(); // now everything is ready, so start the application
	}

}

