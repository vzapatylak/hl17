package hello;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.hl.zapatylak.database.Database;
import com.hl.zapatylak.entity.CutVisit;
import com.hl.zapatylak.entity.Location;
import com.hl.zapatylak.entity.User;
import com.hl.zapatylak.entity.Visit;

import java.math.BigDecimal;
import java.math.RoundingMode;

@RestController
public class GetController {

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getUser(@PathVariable("id") String id) {
		User user = null;

		try {
			user = Database.userTable.get(Integer.valueOf(id));
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

		if (user != null) {
			return new ResponseEntity<String>(new Gson().toJson(user),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/locations/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getLocation(@PathVariable("id") String id) {
		Location location = null;

		try {
			location = Database.locationTable.get(Integer.valueOf(id));
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

		if (location != null) {
			return new ResponseEntity<String>(new Gson().toJson(location),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/visits/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getVisit(@PathVariable("id") String id) {
		Visit visit = null;

		try {
			visit = Database.visitTable.get(Integer.valueOf(id));
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

		if (visit != null) {
			return new ResponseEntity<String>(new Gson().toJson(visit),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/users/{id}/visits", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getUserVisits(
			@PathVariable("id") String id,
			@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "toDistance", required = false) String toDistance) {
		List<Visit> list = null;

		if (Database.userTable.get(Integer.valueOf(id)) == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

		list = Database.visitTableByUser.get(Integer.valueOf(id));
		if(list == null)
		{
			list = new CopyOnWriteArrayList<>();
		}

		try {
			
			if (fromDate != null ) {
				long fDate = Long.valueOf(fromDate);
				list = list.stream().filter(x -> x.getVisitedAt() > fDate)
						.collect(Collectors.toList());
			}
			if (toDate != null ) {
				long tDate = Long.valueOf(toDate);
				list = list.stream().filter(x -> x.getVisitedAt() < tDate)
						.collect(Collectors.toList());
			}
			if (toDistance != null ) {
				int dist = Integer.valueOf(toDistance);
				list = list
						.stream()
						.filter(x -> Database.locationTable.get(
								x.getLocationId()).getDistance() < dist)
						.collect(Collectors.toList());
			}
			if (country != null ) {
				list = list
						.stream()
						.filter(x -> country.equals(Database.locationTable.get(
								x.getLocationId()).getCountry()))
						.collect(Collectors.toList());
			}
			if ( !list.isEmpty()) {
				list.sort(Comparator.comparing(Visit::getVisitedAt));
			}

		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		if (list != null) {
			List<CutVisit> newList = new ArrayList<CutVisit>();
			list.forEach(x -> {
				CutVisit cutVisit = new CutVisit();
				cutVisit.setMark(x.getMark());
				cutVisit.setVisited_at(x.getVisitedAt());
				cutVisit.setPlace((Database.locationTable.get(x.getLocationId())
						.getPlace()));
				newList.add(cutVisit);
			});
			Map<String, List<CutVisit>> map = new HashMap<String, List<CutVisit>>();
			map.put("visits", newList);
			return new ResponseEntity<String>(new Gson().toJson(map),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("{\"visits\": []}", HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/locations/{id}/avg", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getAverageMark(
			@PathVariable("id") String id,
			@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "fromAge", required = false) String fromAge,
			@RequestParam(value = "toAge", required = false) String toAge,
			@RequestParam(value = "gender", required = false) String gender) {
		List<Visit> list = null;

		if (Database.locationTable.get(Integer.valueOf(id)) == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

		list = Database.visitTableByLocation.get(Integer.valueOf(id));
		if(list == null){
			list = new CopyOnWriteArrayList<>();
		}

		try {
			if (fromDate != null) {
				long fDate = Long.valueOf(fromDate);
				list = list.stream().filter(x -> x.getVisitedAt() > fDate)
						.collect(Collectors.toList());
			}
			if (toDate != null && list != null) {
				long tDate = Long.valueOf(toDate);
				list = list.stream().filter(x -> x.getVisitedAt() < tDate)
						.collect(Collectors.toList());
			}

			if (fromAge != null ) {
				
				int fAge = Integer.valueOf(fromAge);
				list = list
						.stream()
						.filter(x -> getAge(Database.userTable.get(
								x.getUserId()).getId()) > fAge)
						.collect(Collectors.toList());
			}

			if (toAge != null ) {
				int tAge = Integer.valueOf(toAge);
				list = list
						.stream()
						.filter(x -> getAge(Database.userTable.get(
								x.getUserId()).getId()) < tAge)
						.collect(Collectors.toList());
			}

			if (gender != null ) {
				list = list
						.stream()
						.filter(x -> gender.equals((Database.userTable.get(x
								.getUserId())).getGender()))
						.collect(Collectors.toList());
			}

		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		if (list != null && !list.isEmpty()) {
			
			int sum = list.stream().mapToInt(x -> x.getMark()).sum();
			Double truncatedDouble = BigDecimal
					.valueOf((double) sum / list.size())
					.setScale(5, RoundingMode.HALF_UP).doubleValue();
			return new ResponseEntity<String>("{\"avg\": "
					+ String.valueOf(truncatedDouble) + "}", HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("{\"avg\": 0.0}", HttpStatus.OK);
		}
	}

	public int getAge(Integer id) {
		Calendar calAgeReferenceDate = Calendar.getInstance();
		calAgeReferenceDate.setTimeInMillis(System.currentTimeMillis());
		Calendar calDateOfBirth = Calendar.getInstance();
		calDateOfBirth.setTimeInMillis((Database.userTable.get(id))
				.getBirthDate() * 1000L);
		return calAgeReferenceDate.get(Calendar.YEAR)
				- calDateOfBirth.get(Calendar.YEAR);
	}

}