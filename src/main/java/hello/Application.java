package hello;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.gson.Gson;
import com.hl.zapatylak.database.Database;
import com.hl.zapatylak.entity.User;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		Database.init();
		System.getProperties().put("server.port", 8080);
		SpringApplication.run(Application.class, args);

	}
}
