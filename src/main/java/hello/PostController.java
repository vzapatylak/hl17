package hello;

import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.transaction.Transactional;

import org.rapidoid.util.D;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.hl.zapatylak.database.Database;
import com.hl.zapatylak.entity.Location;
import com.hl.zapatylak.entity.User;
import com.hl.zapatylak.entity.Visit;

@RestController
public class PostController {

	@RequestMapping(value = "/{item}/new", method = RequestMethod.POST, produces = "application/json")
	@Transactional
	public ResponseEntity<String> add(@PathVariable("item") String item,
			@RequestBody(required = false) String request) {

		if ("users".equals(item)) {
			try {
				User user = (User) createObjectFromRequest(request, "users", true);
				Database.userTable.put(user.getId(), user);
				return new ResponseEntity<String>("{}", HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			}
		}
		if ("locations".equals(item)) {
			try {
				Location location = (Location) createObjectFromRequest(request, "locations", true);
				Database.locationTable.put(location.getId(), location);
				return new ResponseEntity<String>("{}", HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			}
		}

		if ("visits".equals(item)) {
			try {
				Visit visit = (Visit) createObjectFromRequest(request, "visits", true);
				Database.visitTable.put(visit.getId(), visit);
				if (Database.visitTableByUser.get(visit.getUserId()) != null) {
					Database.visitTableByUser.get(visit.getUserId()).add(visit);
				} else {
					CopyOnWriteArrayList<Visit> vList = new CopyOnWriteArrayList<Visit>();
					vList.add(visit);
					Database.visitTableByUser.put(visit.getUserId(), vList);
				}
				if (Database.visitTableByLocation.get(visit.getLocationId()) != null) {
					Database.visitTableByLocation.get(visit.getLocationId()).add(visit);
				} else {
					CopyOnWriteArrayList<Visit> vList = new CopyOnWriteArrayList<Visit>();
					vList.add(visit);
					Database.visitTableByLocation.put(visit.getLocationId(), vList);
				}
				return new ResponseEntity<String>("{}", HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			}
		}

		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/{item}/{id}", method = RequestMethod.POST, produces = "application/json")
	@Transactional
	public ResponseEntity<String> update(@PathVariable("item") String item, @PathVariable("id") String id,
			@RequestBody(required = false) String request) {

		Integer itemId = null;
		try {
			itemId = Integer.valueOf(id);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		try {
			if ("users".equals(item)) {
				User user = (User) createObjectFromRequestNullAllowed(request, "users", itemId);

				if (Database.userTable.get(user.getId()) == null) {
					return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
				} else {
					User userToUpdate = Database.userTable.get(user.getId());
					if (user.getBirthDate() != 0) {
						userToUpdate.setBirthDate(user.getBirthDate());
					}
					if (user.getEmail() != null) {
						userToUpdate.setEmail(user.getEmail());
					}
					if (user.getFirstName() != null) {
						userToUpdate.setFirstName(user.getFirstName());
					}
					if (user.getGender() != null) {
						userToUpdate.setGender(user.getGender());
					}
					if (user.getLastName() != null) {
						userToUpdate.setLastName(user.getLastName());
					}
					Database.userTable.put(userToUpdate.getId(), userToUpdate);
					return new ResponseEntity<String>("{}", HttpStatus.OK);
				}
			}
			if ("locations".equals(item)) {
				Location location = (Location) createObjectFromRequestNullAllowed(request, "locations", itemId);
				if (Database.locationTable.get(location.getId()) == null) {
					return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
				} else {
					Location locationToUpdate = Database.locationTable.get(location.getId());
					if (location.getDistance() != 0) {
						locationToUpdate.setDistance(location.getDistance());
					}
					if (location.getCity() != null) {
						locationToUpdate.setCity(location.getCity());
					}
					if (location.getCountry() != null) {
						locationToUpdate.setCountry(location.getCountry());
					}
					if (location.getPlace() != null) {
						locationToUpdate.setPlace(location.getPlace());
					}
					Database.locationTable.put(locationToUpdate.getId(), locationToUpdate);
					return new ResponseEntity<String>("{}", HttpStatus.OK);
				}
			}
			if ("visits".equals(item)) {
				Visit visit = (Visit) createObjectFromRequestNullAllowed(request, "visits", itemId);
				if (Database.visitTable.get(visit.getId()) == null) {
					return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
				} else {
					Visit visitToUpdate = Database.visitTable.get(visit.getId());
					// Visit visitToRemove =
					// Database.visitTable.get(visit.getId());
					Visit visitToRemove = new Visit();
					visitToRemove.setId(visitToUpdate.getId());
					visitToRemove.setLocationId(visitToUpdate.getLocationId());
					visitToRemove.setMark(visitToRemove.getMark());
					visitToRemove.setUserId(visitToUpdate.getUserId());
					visitToRemove.setVisitedAt(visitToUpdate.getVisitedAt());
					visitToUpdate.setMark(visit.getMark());
					if (visit.getLocationId() > 0) {
						visitToUpdate.setLocationId(visit.getLocationId());
					}
					if (visit.getUserId() > 0) {
						visitToUpdate.setUserId(visit.getUserId());
					}
					if (visit.getVisitedAt() > 0) {
						visitToUpdate.setVisitedAt(visit.getVisitedAt());
					}
					Database.visitTable.put(visitToUpdate.getId(), visitToUpdate);
					Database.visitTableByLocation.get(visitToRemove.getLocationId()).remove(visitToRemove);
					if (Database.visitTableByLocation.get(visitToUpdate.getLocationId()) != null) {
						Database.visitTableByLocation.get(visitToUpdate.getLocationId()).add(visitToUpdate);
					} else {
						CopyOnWriteArrayList<Visit> list = new CopyOnWriteArrayList<Visit>();
						list.add(visitToUpdate);
						Database.visitTableByLocation.put(visitToUpdate.getLocationId(), list);
					}
					Database.visitTableByUser.get(visitToRemove.getUserId()).remove(visitToRemove);
					if (Database.visitTableByUser.get(visitToUpdate.getUserId()) != null) {
						Database.visitTableByUser.get(visitToUpdate.getUserId()).add(visitToUpdate);
					} else {
						CopyOnWriteArrayList<Visit> list = new CopyOnWriteArrayList<Visit>();
						list.add(visitToUpdate);
						Database.visitTableByUser.put(visitToUpdate.getUserId(), list);
					}
					return new ResponseEntity<String>("{}", HttpStatus.OK);
				}

			}
		} catch (Exception e) {
			// e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}

	public Object createObjectFromRequest(String request, String type, boolean flag) {

		if (request != null) {
			if (request.contains("null")) {
				return null;
			}
			if ("users".equals(type)) {
				try {
					Map map = new Gson().fromJson(request, Map.class);
					User user = new User();
					user.setId(getLatestId());
					Long birthDate = ((Double) map.get("birth_date")).longValue();
					if (birthDate != null) {
						user.setBirthDate(birthDate);
					} else {
						return null;
					}
					String email = map.get("email").toString();
					if (email != null && email.length() < 101 && email.matches(
							"^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$")) {
						user.setEmail(email);
					} else {
						return null;
					}
					String firstName = map.get("first_name").toString();
					if (firstName != null && firstName.length() < 51) {
						user.setFirstName(firstName);
					} else {
						return null;
					}
					String lastName = map.get("last_name").toString();
					if (lastName != null && lastName.length() < 51) {
						user.setLastName(lastName);
					} else {
						return null;
					}
					String gender = map.get("gender").toString();
					if (gender != null && "f".equals(gender) || "m".equals(gender)) {
						user.setGender(map.get("gender").toString());
					} else {
						return null;
					}
					return user;
				} catch (Exception e) {
					return null;
				}

			}
			if ("locations".equals(type)) {
				try {
					Map map = new Gson().fromJson(request, Map.class);
					Location location = new Location();
					location.setId(getLatestId());
					String city = map.get("city").toString();
					if (city != null && city.length() < 51) {
						location.setCity(city);
					} else {
						return null;
					}
					String country = map.get("country").toString();
					if (country != null && country.length() < 51) {
						location.setCountry(country);
					} else {
						return null;
					}
					String place = map.get("place").toString();
					if (place != null && place.length() < 51) {
						location.setPlace(place);
					} else {
						return null;
					}
					int distance = ((Double) (map.get("distance"))).intValue();
					if (distance > 0) {
						location.setDistance(distance);
					} else {
						return null;
					}
					return location;
				} catch (Exception e) {
					return null;
				}

			}
			if ("visits".equals(type)) {
				try {
					Map map = new Gson().fromJson(request, Map.class);
					Visit visit = new Visit();
					visit.setId(getLatestId());
					int locationId = ((Double) (map.get("location"))).intValue();
					if (locationId > 0 && Database.locationTable.get(locationId) != null) {
						visit.setLocationId(locationId);
					} else {
						return null;
					}
					int userId = ((Double) (map.get("user"))).intValue();
					if (userId > 0 && Database.userTable.get(userId) != null) {
						visit.setUserId(userId);
					} else {
						return null;
					}
					int mark = ((Double) (map.get("mark"))).intValue();
					if (mark >= 0 && mark < 6) {
						visit.setMark(mark);
					} else {
						return null;
					}
					Long visitedAt = ((Double) map.get("visited_at")).longValue();
					if (visitedAt != null) {
						visit.setVisitedAt(visitedAt);
					} else {
						return null;
					}
					return visit;
				} catch (Exception e) {
					return null;
				}

			}
		}
		return null;
	}

	public Object createObjectFromRequestNullAllowed(String request, String type, Integer id) {

		if (request != null) {
			if (request.contains("null")) {
				return null;
			}
			if ("users".equals(type)) {
				try {
					Map map = new Gson().fromJson(request, Map.class);

					User user = new User();
					user.setId(id);
					Long birthDate = ((Double) map.get("birth_date")) == null ? null
							: ((Double) map.get("birth_date")).longValue();
					if (birthDate != null) {
						user.setBirthDate(birthDate);
					} else {
						user.setBirthDate(0);
					}

					String email = map.get("email") == null ? null : map.get("email").toString();
					if (email == null || email != null && email.length() < 101 && email.matches(
							"^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$")) {
						user.setEmail(email);
					} else {
						return null;
					}
					String firstName = map.get("first_name") == null ? null : map.get("first_name").toString();
					if (firstName == null || firstName != null && firstName.length() < 51) {
						user.setFirstName(firstName);
					} else {
						return null;
					}
					String lastName = map.get("last_name") == null ? null : map.get("last_name").toString();
					if (lastName == null || lastName != null && lastName.length() < 51) {
						user.setLastName(lastName);
					} else {
						return null;
					}
					String gender = map.get("gender") == null ? null : map.get("gender").toString();
					if (gender == null || gender != null && "f".equals(gender) || "m".equals(gender)) {
						user.setGender(gender);
					} else {
						return null;
					}
					return user;

				} catch (Exception e) {
					return null;
				}

			}
			if ("locations".equals(type)) {
				try {
					Map map = new Gson().fromJson(request, Map.class);
					Location location = new Location();
					location.setId(id);

					String city = map.get("city") == null ? null : map.get("city").toString();
					if (city == null || city != null && city.length() < 51) {
						location.setCity(city);
					} else {
						return null;
					}
					String country = map.get("country") == null ? null : map.get("country").toString();
					if (country == null || country != null && country.length() < 51) {
						location.setCountry(country);
					} else {
						return null;
					}
					String place = map.get("place") == null ? null : map.get("place").toString();
					if (place == null || place != null && place.length() < 51) {
						location.setPlace(place);
					} else {
						return null;
					}

					Integer distance = ((Double) map.get("distance")) == null ? null
							: ((Double) map.get("distance")).intValue();
					if (distance != null && distance > 0) {
						location.setDistance(distance);
					} else {
						if (distance != null) {
							return null;
						}
					}
					return location;
				} catch (Exception e) {
					return null;
				}
			}
			if ("visits".equals(type)) {
				try {
					Map map = new Gson().fromJson(request, Map.class);
					Visit visit = new Visit();
					visit.setId(id);
					Integer locationId = ((Double) map.get("location")) == null ? null
							: ((Double) map.get("location")).intValue();
					if (locationId != null) {
						visit.setLocationId(locationId);
					}
					if (locationId != null && Database.locationTable.get(visit.getLocationId()) == null) {
						return null;
					}
					Integer mark = ((Double) map.get("mark")) == null ? null : ((Double) map.get("mark")).intValue();
					if (mark != null && mark >= 0 && mark <= 5) {
						visit.setMark(mark);
					} else {
						if (mark != null) {
							return null;
						}
					}
					Integer userId = ((Double) map.get("user")) == null ? null : ((Double) map.get("user")).intValue();
					if (userId != null) {
						visit.setUserId(userId);
					}
					if ((userId != null && Database.userTable.get(visit.getUserId()) == null)) {
						return null;
					}
					Long visitedAt = ((Double) map.get("visited_at")) == null ? null
							: ((Double) map.get("visited_at")).longValue();
					if (visitedAt != null) {
						visit.setVisitedAt(visitedAt);
					}
					return visit;
				} catch (Exception e) {
					return null;
				}
			}
		}
		return null;
	}

	public Integer getLatestId() {
		Lock lock = new ReentrantLock();
		lock.lock();
		Integer id = Database.scheme.get("lastID") + 1;
		Database.scheme.put("lastID", id);
		lock.unlock();
		return id;
	}

}
